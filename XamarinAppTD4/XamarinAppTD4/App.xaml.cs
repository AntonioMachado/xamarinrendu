﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinAppTD4.Services;
using XamarinAppTD4.Views;

namespace XamarinAppTD4
{
    public partial class App : Application
    {
        public static RestManager restManager { get; private set; }

        public App()
        {
            InitializeComponent();
            restManager = new RestManager(new RestService());
            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
