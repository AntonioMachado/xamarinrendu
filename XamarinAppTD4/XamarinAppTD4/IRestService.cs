﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;
using XamarinAppTD4.modele;

namespace XamarinAppTD4
{
    public interface IRestService
    {
        Task<String> getAPI();

        Task<bool> login(string email, string pwd);

        Task<bool> createAct(string email, string pwd, string fName, string lName);

        Task<HttpResponseMessage> getProfil();

        Task<HttpResponseMessage> setProfil(UpdateProfileRequest profile);

        Task<HttpResponseMessage> getImage(int? id);

        Task<HttpResponseMessage> getPlaces(int id);

        Task<HttpResponseMessage> getPlacesSummary();

        Task<bool> sendComment(int placeId, string comment);

        Task<int> uploadImage(byte[] imageData);

        Task<bool> addPlace(string title, string description, Position pos, int imageId);
     }
}
