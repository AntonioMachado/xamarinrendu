﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms.Maps;
using XamarinAppTD4.modele;

namespace XamarinAppTD4.Services
{
    public class RestService : IRestService
    {
        private HttpClient client;
        private string API_BASE_URL = "https://td-api.julienmialon.com";
        private LoginResult loginResult;

        public RestService()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
        }

        public async Task<bool> login(string email, string pwd)
        {
            try
            {
                var login = new LoginRequest();
                login.Email = email;
                login.Password = pwd;
                var json = JsonConvert.SerializeObject(login);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await client.PostAsync(API_BASE_URL + "/auth/login", content);
                if (response.IsSuccessStatusCode)
                {
                    var content2 = await response.Content.ReadAsStringAsync();
                    loginResult = JsonConvert.DeserializeObject<Response<LoginResult>>(content2).Data;
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(loginResult.TokenType, loginResult.AccessToken);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            return false;

        }

        public async Task<bool> createAct(string email, string pwd, string fName, string lName)
        {
            var signUp = new RegisterRequest();
            signUp.Email = email;
            signUp.Password = pwd;
            signUp.FirstName = fName;
            signUp.LastName = lName;
            var content = new StringContent(JsonConvert.SerializeObject(signUp), Encoding.UTF8, "application/json");
            var response = await client.PostAsync(API_BASE_URL + "/auth/register", content);
            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            return false;
        }

        public async Task<HttpResponseMessage> getProfil()
        {
            var uri = new Uri(string.Format(API_BASE_URL, string.Empty));
            try
            {
                return await client.GetAsync(API_BASE_URL + "/me");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error");
            }
            return null;
        }

        public async Task<HttpResponseMessage> setProfil(UpdateProfileRequest profile)
        {
            try
            {
                var json = JsonConvert.SerializeObject(profile);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var method = new HttpMethod("PATCH");
                var request = new HttpRequestMessage(method, API_BASE_URL + "/me")
                {
                    Content = content
                };
                return await client.SendAsync(request);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"             ERROR {0}", ex.Message);
            }
            return null;
        }

        public async Task<String> getAPI()
        {
            var uri = new Uri(string.Format(API_BASE_URL, string.Empty));
            try
            {
                var response = await client.GetAsync(API_BASE_URL);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<string>(content);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error");
            }
            return "";
        }

        public async Task<HttpResponseMessage> getImage(int? id)
        {
            try
            {
                return await client.GetAsync(API_BASE_URL + "/images" + id);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error");
            }
            return null;
        }

        public async Task<HttpResponseMessage> getPlacesSummary()
        {
            try
            {
                return await client.GetAsync(API_BASE_URL + "/places");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error");
            }
            return null;
        }

        public async Task<HttpResponseMessage> getPlaces(int id)
        {
            try
            {
                return await client.GetAsync(API_BASE_URL + "/places/" + id);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error");
            }
            return null;
        }

        public async Task<bool> sendComment(int placeId, string message)
        {
            var item = new CreateCommentRequest();
            item.Text = message;
            var content = new StringContent(JsonConvert.SerializeObject(item), Encoding.UTF8, "application/json");
            var response = await client.PostAsync(API_BASE_URL + "/places/" + placeId + "/comments", content);
            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            return false;
        }

        public async Task<int> uploadImage(byte[] imageData)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "https://td-api.julienmialon.com/images");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", loginResult.AccessToken);

            MultipartFormDataContent requestContent = new MultipartFormDataContent();

            var imageContent = new ByteArrayContent(imageData);
            imageContent.Headers.ContentType = MediaTypeHeaderValue.Parse("image/jpeg");

            // Le deuxième paramètre doit absolument être "file" ici sinon ça ne fonctionnera pas
            requestContent.Add(imageContent, "file", "file.jpg");

            request.Content = requestContent;

            HttpResponseMessage response = await client.SendAsync(request);

            var imageId = JsonConvert.DeserializeObject<Response<ImageItem>>(await response.Content.ReadAsStringAsync()).Data.Id;

            if (response.IsSuccessStatusCode)
            {
                return imageId;
            }
            return -1;
        }

        public async Task<bool> addPlace(string title, string description, Position pos, int imageId)
        {
            var item = new CreatePlaceRequest();
            item.Title = title;
            item.Description = description;
            item.Latitude = pos.Latitude;
            item.Longitude = pos.Longitude;
            item.ImageId = imageId;

            var content = new StringContent(JsonConvert.SerializeObject(item), Encoding.UTF8, "application/json");
            var response = await client.PostAsync(API_BASE_URL + "/places", content);
            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            return false;
        }
    }
}
