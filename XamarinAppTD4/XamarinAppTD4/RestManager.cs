﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;
using XamarinAppTD4.modele;

namespace XamarinAppTD4
{
    public class RestManager
    {
        IRestService restService;

        public RestManager(IRestService service)
        {
            restService = service;
        }

        public Task<string> GetAPIAsync()
        {
            return restService.getAPI();
        }

        public Task<bool> login(string email, string pwd)
        {
            return restService.login(email, pwd);
        }

        public Task<bool> createAct(string email, string pwd, string fName, string lName)
        {
            return restService.createAct(email, pwd, fName, lName);
        }

        public Task<HttpResponseMessage> getProfil(){
            return restService.getProfil();
        }

        public Task<HttpResponseMessage> setProfil(UpdateProfileRequest profile)
        {
            return restService.setProfil(profile);
        }

        public Task<HttpResponseMessage> getImage(int? id)
        {
            return restService.getImage(id);
        }

        public Task<HttpResponseMessage> getPlacesSummary()
        {
            return restService.getPlacesSummary();
        }

        public Task<HttpResponseMessage> getPlaces(int id)
        {
            return restService.getPlaces(id);
        }

        public Task<bool> sendComment(int placeId, string comment)
        {
            return restService.sendComment(placeId, comment);
        }

        public Task<int> uploadImage(byte[] imageData)
        {
            return restService.uploadImage(imageData);
        }

        public Task<bool> addPlace(string title, string description, Position pos, int imageId)
        {
            return restService.addPlace(title, description, pos, imageId);
        }
    }
}
