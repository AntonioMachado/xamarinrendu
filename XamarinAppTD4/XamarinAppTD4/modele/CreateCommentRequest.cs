using Newtonsoft.Json;

namespace XamarinAppTD4.modele
{
	public class CreateCommentRequest
	{
		[JsonProperty("text")]
		public string Text { get; set; }
	}
}