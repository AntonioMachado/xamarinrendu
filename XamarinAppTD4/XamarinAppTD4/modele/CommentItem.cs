using System;
using Newtonsoft.Json;

namespace XamarinAppTD4.modele
{
	public class CommentItem
	{
		[JsonProperty("date")]
		public DateTime Date { get; set; }
		
		[JsonProperty("author")]
		public UserItem Author { get; set; }
		
		[JsonProperty("text")]
		public string Text { get; set; }

        public string DateString { get => Date.ToString(); }

        public string ImageUrl { get => "https://td-api.julienmialon.com/Images/" + Author.ImageId; }

        public string FirstName { get => Author.FirstName; }

        public string LastName { get => Author.LastName; }

        public string Email { get => Author.Email; }
    }
}