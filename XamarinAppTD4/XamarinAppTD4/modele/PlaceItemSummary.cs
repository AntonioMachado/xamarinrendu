using Newtonsoft.Json;

namespace XamarinAppTD4.modele
{
	public class PlaceItemSummary
	{
		[JsonProperty("id")]
		public int Id { get; set; }
		
		[JsonProperty("title")]
		public string Title { get; set; }
		
		[JsonProperty("description")]
		public string Description { get; set; }
		
		[JsonProperty("image_id")]
		public int ImageId { get; set; }
		
		[JsonProperty("latitude")]
		public double Latitude { get; set; }
		
		[JsonProperty("longitude")]
		public double Longitude { get; set; }

        public string ImageSrc
        {
            get => "https://td-api.julienmialon.com/Images/" + ImageId;
        }
    }
}