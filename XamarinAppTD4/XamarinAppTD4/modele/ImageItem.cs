using Newtonsoft.Json;

namespace XamarinAppTD4.modele
{
	public class ImageItem
	{
		[JsonProperty("id")]
		public int Id { get; set; }
	}
}