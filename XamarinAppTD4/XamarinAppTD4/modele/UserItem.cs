using Newtonsoft.Json;

namespace XamarinAppTD4.modele
{
    public class UserItem
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        
        [JsonProperty("email")]
        public string Email { get; set; }
        
        [JsonProperty("image_id")]
        public int? ImageId { get; set; }

        public string ImageUrl { get {
                if (ImageId != null)
                    return "https://td-api.julienmialon.com/Images/" + ImageId;
                else
                    return "https://td-api.julienmialon.com/Images/1";

            }
        }
    }
}