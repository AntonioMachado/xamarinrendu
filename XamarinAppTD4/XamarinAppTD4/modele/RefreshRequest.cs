using Newtonsoft.Json;

namespace XamarinAppTD4.modele
{
    public class RefreshRequest
    {
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
    }
}