﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using XamarinAppTD4.ViewModel;

namespace XamarinAppTD4.Views
{
    public partial class AddPlacePage : ContentPage
    {
        public AddPlacePage()
        {
            InitializeComponent();
            BindingContext = new AddPlacePageViewModel(this, Navigation);
        }
    }
}
