﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using XamarinAppTD4.modele;
using XamarinAppTD4.ViewModel;

namespace XamarinAppTD4.Views
{
    public partial class PlaceItemPage : ContentPage
    {
        public PlaceItemPage() { }

        public PlaceItemPage(PlaceItemSummary item)
        {
            InitializeComponent();
            BindingContext = new PlaceItemPageViewModel(item, placeItemMap, Navigation); 
        }
    }
}
