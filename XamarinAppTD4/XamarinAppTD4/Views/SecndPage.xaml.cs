﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using XamarinAppTD4.modele;
using XamarinAppTD4.ViewModel;

namespace XamarinAppTD4.Views
{
    public partial class SecndPage : ContentPage
    {
        public SecndPage()
        {
            InitializeComponent();
            BindingContext = new SecndPageViewModel(Navigation);

        }

        public async void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            PlaceItemSummary item = e.Item as PlaceItemSummary;
            if (item != null)
                await Navigation.PushAsync(new PlaceItemPage(item));
        }
    }
}
