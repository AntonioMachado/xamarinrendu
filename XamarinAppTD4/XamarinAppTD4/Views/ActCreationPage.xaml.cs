﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using XamarinAppTD4.ViewModel;

namespace XamarinAppTD4.Views
{
    public partial class ActCreationPage : ContentPage
    {
        public ActCreationPage()
        {
            InitializeComponent();
            BindingContext = new ActCreationPageViewModel(Navigation);
        }
    }
}
