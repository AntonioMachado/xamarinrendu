﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using Newtonsoft.Json;
using Xamarin.Forms;
using XamarinAppTD4.modele;
using XamarinAppTD4.ViewModel;

namespace XamarinAppTD4.Views
{
    public partial class ProfilPage : ContentPage
    {
        public ProfilPage()
        {
            InitializeComponent();
            BindingContext = new ProfilPageViewModel(Navigation);
        }
    }
}
