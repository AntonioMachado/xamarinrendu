﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using XamarinAppTD4.ViewModel;

namespace XamarinAppTD4.Views
{
    public partial class AddCommentPage : ContentPage
    {
        public AddCommentPage(int placeId)
        {
            InitializeComponent();
            BindingContext = new AddCommentPageViewModel(Navigation, placeId);
        }
    }
}
