﻿using System;
using System.IO;
using System.Net.Http;
using System.Windows.Input;
using Newtonsoft.Json;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Storm.Mvvm;
using Xamarin.Forms;
using XamarinAppTD4.modele;

namespace XamarinAppTD4.ViewModel
{
    public class ProfilPageViewModel : ViewModelBase
    {
        public ICommand SaveBtnCommand { get; private set; }
        public ICommand ChangeProfilPickCommand { get; private set; }

        private byte[] imageData;
        private INavigation _navigation;
        private UserItem profilContent;

        public string _imageUrl;
        public string ImageUrl
        {
            set => SetProperty(ref _imageUrl, value);
            get => _imageUrl;
        }

        public string _nameEntry;
        public string NameEntry
        {
            set => SetProperty(ref _nameEntry, value);
            get => _nameEntry;
        }

        public string _firstNameEntry;
        public string FirstNameEntry
        {
            set => SetProperty(ref _firstNameEntry, value);
            get => _firstNameEntry;
        }

        public string _emailEntry;
        public string EmailEntry
        {
            set => SetProperty(ref _emailEntry, value);
            get => _emailEntry;
        }

        public ProfilPageViewModel(INavigation navigation)
        {
            _navigation = navigation;
            SaveBtnCommand = new Command(() => saveBtnClick());
            ChangeProfilPickCommand = new Command(() => changeProfilPick());
            getProfil();
        }

        private async void changeProfilPick()
        {
            await CrossMedia.Current.Initialize();

            var mediaOptions = new PickMediaOptions() { PhotoSize = PhotoSize.Full, SaveMetaData=false};

            var selectedImage = await CrossMedia.Current.PickPhotoAsync(mediaOptions);
            var memStream = new MemoryStream();
            selectedImage.GetStream().CopyTo(memStream);
            imageData = memStream.ToArray();

        }

        private async void saveBtnClick()
        {
            int imageId;
            var item = new UpdateProfileRequest();
            item.FirstName = FirstNameEntry;
            item.LastName = NameEntry;
            if(imageData != null || imageData.Length > 0)
            {
                imageId = await App.restManager.uploadImage(imageData);
                if (imageId != -1)
                    item.ImageId = imageId;
            }
            var response = await App.restManager.setProfil(item);
            getProfil();
        }

        public async void getProfil()
        {
            HttpResponseMessage res = await App.restManager.getProfil();
            if (res.IsSuccessStatusCode)
            {
                profilContent = JsonConvert.DeserializeObject<Response<UserItem>>(await res.Content.ReadAsStringAsync()).Data;

                if (profilContent.ImageId != null)
                    ImageUrl = profilContent.ImageUrl;
               
                NameEntry = profilContent.FirstName;
                EmailEntry = profilContent.Email;
                FirstNameEntry = profilContent.LastName;
            }
        }
    }
}
