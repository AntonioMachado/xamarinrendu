﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Newtonsoft.Json;
using Storm.Mvvm;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using XamarinAppTD4.modele;
using XamarinAppTD4.Views;

namespace XamarinAppTD4.ViewModel
{
    public class PlaceItemPageViewModel : ViewModelBase
    {
        private PlaceItem _placeItem;
        private INavigation _navigation;
        public ICommand AddComment { get; set; }

        private Map _placeItemMap;
        public Map PlaceItemMap { get => _placeItemMap; set => SetProperty(ref _placeItemMap, value); }

        private List<CommentItem> _comments;
        public List<CommentItem> Comments { get => _comments; set => SetProperty(ref _comments, value); }

        private string _title;
        public string Title { get => _title; set => SetProperty(ref _title, value); }

        private string _description;
        public string Description { get => _description; set => SetProperty(ref _description, value); }

        private string _imageUrl;
        public string ImageUrl { get => _imageUrl; set => SetProperty(ref _imageUrl, value); }

        public PlaceItemPageViewModel(PlaceItemSummary placeItemSummary, Map placeItemMap, INavigation navigation)
        {
            getPlaceItem(placeItemSummary.Id);
            PlaceItemMap = placeItemMap;
            AddComment = new Command(() => addComment());
            _navigation = navigation;
        }

        public async void addComment()
        {
            await _navigation.PushAsync(new AddCommentPage(_placeItem.Id));
        }

        public async void getPlaceItem(int id)
        {
            var res = await App.restManager.getPlaces(id);
            if (res.IsSuccessStatusCode)
            {
                var json = await res.Content.ReadAsStringAsync();
                _placeItem = JsonConvert.DeserializeObject<Response<PlaceItem>>(json).Data;
                refreshInfos(_placeItem);
            }
        }

        public void refreshInfos(PlaceItem place)
        {
            var position = new Position(place.Latitude, place.Longitude); // Latitude, Longitude
            PlaceItemMap.MoveToRegion(MapSpan.FromCenterAndRadius(
                position, Distance.FromMiles(0.2)));
            var pin = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = place.Title
            };
            PlaceItemMap.Pins.Add(pin);
            Comments = place.Comments;
            Title = place.Title;
            Description = place.Description;
            ImageUrl = "https://td-api.julienmialon.com/Images/" + place.ImageId;
        }
    }
}
