﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using Plugin.Geolocator;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Storm.Mvvm;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using XamarinAppTD4.Views;

namespace XamarinAppTD4.ViewModel
{
    public class AddPlacePageViewModel : ViewModelBase
    {
        public ICommand ShowPos { get; set; }
        public ICommand ShowActualPos { get; set; }
        public ICommand TakePhoto { get; set; }
        public ICommand SavePlace { get; set; }

        private INavigation _navigation;
        private int lat;
        private int lon;
        private bool pos;
        private int imageId = -1;
        private Position positionToSave;

        public Map Map { get; private set; }

        private string _title;
        public string Title { get => _title; set => SetProperty(ref _title, value); }

        private string _description;
        public string Description { get => _description; set => SetProperty(ref _description, value); }

        private string _imageUrl;
        public string ImageUrl { get => _imageUrl; set => SetProperty(ref _imageUrl, value); }

        private string _latitude;
        public string Latitude { get => _latitude; set => SetProperty(ref _latitude, value); }

        private string _longitude;
        public string Longitude { get => _longitude; set => SetProperty(ref _longitude, value); }

        private string _errorText;
        public string ErrorText { get => _errorText; set => SetProperty(ref _errorText, value); }

        private Position position;
        private AddPlacePage _context;

        public AddPlacePageViewModel(AddPlacePage context, INavigation navigation)
        {
            _navigation = navigation;
            ShowPos = new Command(() => showPos());
            ShowActualPos = new Command(() => showActualPos());
            TakePhoto = new Command(() => takePhoto());
            SavePlace = new Command(() => savePlace());
            Map = new Map()
            {
                IsShowingUser = true,
                MapType = MapType.Street,
                HeightRequest = 100,
                WidthRequest = 100
            };
            _context = context;
        }

        private async Task showActualPos()
        {
            if (!CrossGeolocator.Current.IsGeolocationAvailable || !CrossGeolocator.Current.IsGeolocationEnabled)
                ErrorText = "La géolocalisation n'est pas activée/authorisée";
            var position = await CrossGeolocator.Current.GetPositionAsync(TimeSpan.FromSeconds(20), null, true);
            Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(position.Latitude, position.Longitude), Distance.FromMiles(0.2)));
            positionToSave = new Position(position.Latitude, position.Longitude);
            pos = true;
        }

        private async Task showPos()
        {
            try
            {
                lat = int.Parse(Latitude);
                lon = int.Parse(Longitude);
                positionToSave = new Position(lat, lon);
                pos = true;
            }
            catch (Exception e)
            {
                ErrorText = "Veuillez rentrer des entiers pour la longitude et la latitude";
                pos = false;
            }
            Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(lat, lon), Distance.FromMiles(0.2)));
        }

        private async Task takePhoto()
        {

            await CrossMedia.Current.Initialize();
            MediaFile selectedImage;
            var mediaOptions = new PickMediaOptions() { PhotoSize = PhotoSize.Full, SaveMetaData = false };
            selectedImage = await CrossMedia.Current.PickPhotoAsync(mediaOptions);
            var memStream = new MemoryStream();
            selectedImage.GetStream().CopyTo(memStream);
            var imageData = memStream.ToArray();
            imageId = await App.restManager.uploadImage(imageData);
            ImageUrl = "https://td-api.julienmialon.com/Images/" + imageId;
        }

        private async Task savePlace()
        {
            if((Title !=null || Description != null) && (Title.Length > 0 || Description.Length > 0) || pos)
            {
                if (await App.restManager.addPlace(Title, Description, positionToSave, imageId))
                    await _navigation.PopAsync();
            }
        }
    }
}
