﻿using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Windows.Input;
using Newtonsoft.Json;
using Storm.Mvvm;
using Xamarin.Forms;
using XamarinAppTD4.modele;
using XamarinAppTD4.Views;

namespace XamarinAppTD4.ViewModel
{
    public class SecndPageViewModel : ViewModelBase
    {

        public ICommand AddPlace { get; set; }
        private INavigation _navigation;
        private ObservableCollection<PlaceItemSummary> _placesItemSummary;
        public ObservableCollection<PlaceItemSummary> PlacesItemSummary { get => _placesItemSummary; set => SetProperty(ref _placesItemSummary, value); }

        public SecndPageViewModel(INavigation navigation)
        {
            _navigation = navigation;
            PlacesItemSummary = new ObservableCollection<PlaceItemSummary>();
            AddPlace = new Command(() => moveToAddPlacePage());
            getPlaces();
        }

        public async void getPlaces()
        {
            HttpResponseMessage response = await App.restManager.getPlacesSummary();
            if (response.IsSuccessStatusCode)
            {
                PlacesItemSummary.Clear();
                ObservableCollection<PlaceItemSummary> newPlaces = JsonConvert.DeserializeObject<Response<ObservableCollection<PlaceItemSummary>>>(await response.Content.ReadAsStringAsync()).Data;
                foreach(PlaceItemSummary placeSum in newPlaces)
                {
                    PlacesItemSummary.Add(placeSum);
                }
            }
        }

        private async void moveToAddPlacePage()
        {
            await _navigation.PushAsync(new AddPlacePage());
        }
    }
}
