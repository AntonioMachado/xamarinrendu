﻿using System;
using System.Windows.Input;
using Storm.Mvvm;
using Xamarin.Forms;

namespace XamarinAppTD4.ViewModel
{
    public class ActCreationPageViewModel : ViewModelBase
    {
        public string emailEntry { get; set; }
        public string pwdEntry { get; set; }
        public string firstNameEntry { get; set; }
        public string lastNameEntry { get; set; }

        public string _errorText;
        public string ErrorText
        {
            set => SetProperty(ref _errorText, value);
            get => _errorText;
        }
        public ICommand ActCreationBtnCommand { get; private set; }
        private INavigation _navigation;

        public ActCreationPageViewModel()
        {

        }

        public ActCreationPageViewModel(INavigation navigation)
        {
            _navigation = navigation;
            ActCreationBtnCommand = new Command(() => createAccount());
        }

        public async void createAccount()
        {
            ErrorText = "";
            if ((emailEntry== null || pwdEntry== null || firstNameEntry == null || lastNameEntry== null) || (emailEntry.Length < 1 || pwdEntry.Length < 1 || firstNameEntry.Length < 1 || lastNameEntry.Length < 1))
                ErrorText = "Pour vous inscrire, veuillez remplir les champs email, mot de passe, nom et prénom.";
            else
            {
                if (await App.restManager.createAct(emailEntry, pwdEntry, firstNameEntry, lastNameEntry))
                {
                    await _navigation.PopAsync();
                }
                else
                {
                    ErrorText = "la création de compte a échoué.";
                }
            }
        }
    }
}
