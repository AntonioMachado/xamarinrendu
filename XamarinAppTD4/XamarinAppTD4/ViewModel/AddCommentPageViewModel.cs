﻿using System;
using System.Windows.Input;
using Storm.Mvvm;
using Xamarin.Forms;

namespace XamarinAppTD4.ViewModel
{
    public class AddCommentPageViewModel : ViewModelBase
    {
        private int _placeId;
        public string ErrorText { get; set; }
        public string firstNameEntry { get; set; }
        public string lastNameEntry { get; set; }
        public string CommentEntry { get; set; }
        public ICommand SendComment { get; set; }

        private INavigation _navigation;

        public AddCommentPageViewModel(INavigation navigation, int placeId)
        {
            _navigation = navigation;
            _placeId = placeId;
            SendComment = new Command(() => sendComment());
        }

        private async void sendComment()
        {
            if (await App.restManager.sendComment(_placeId, CommentEntry))
                await _navigation.PopAsync();
            else
                ErrorText = "Le commentaire n'a pas pu être envoyé";
        }
    }
}
