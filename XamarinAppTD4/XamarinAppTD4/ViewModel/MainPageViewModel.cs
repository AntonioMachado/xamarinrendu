﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Input;
using Storm.Mvvm;
using Xamarin.Forms;
using XamarinAppTD4.Services;
using XamarinAppTD4.Views;
using System.Net.Http;


namespace XamarinAppTD4.ViewModel
{
    public class MainPageViewModel : ViewModelBase
    {
        public ICommand CreateActBtnCommand { get; private set; }
        public ICommand LoginBtnCommand { get; private set; }
        private INavigation _navigation;

        //public event PropertyChangedEventHandler PropertyChanged;
        public string labelText { get; set; }
        public string emailEntry { get; set; }
        public string pwdEntry { get; set; }
        public UriImageSource _logoSource;
        public UriImageSource LogoSource
        {
            set => SetProperty(ref _logoSource, value);
            get => _logoSource;
        }
        public string _actNotFoundText;
        public string ActNotFound
        {
            set => SetProperty(ref _actNotFoundText, value);
            get => _actNotFoundText;
        }

        public MainPageViewModel(INavigation navigation)
        {
            _navigation = navigation;
            LoginBtnCommand = new Command(() => login());
            CreateActBtnCommand = new Command(() => createAccount());

            labelText = "Welcome !";
            LogoSource = new UriImageSource
            {
                Uri = new Uri("https://blog.clever-age.com/wp-content/uploads/sites/2/2014/10/Xamarin-logo-hexagon-blue-200x200.png"),
                CachingEnabled = true,
                CacheValidity = new TimeSpan(5, 0, 0, 0),
            };
        }

        public async void login()
        {
            ActNotFound = "";
            if (emailEntry.Length < 1 || pwdEntry.Length < 1)
                ActNotFound = "Pour vous connecter, veuillez remplir les champs email et mot de passe.";
            else
            {
                bool response = await App.restManager.login(emailEntry, pwdEntry);
                if (response)
                {
                    await _navigation.PushAsync(new TabbedViewPage());
                }
                else
                {
                    ActNotFound = "Identifiant/Mot de passe incorrecte";
                }
            }
        }

        public async void createAccount()
        {
            await _navigation.PushAsync(new ActCreationPage());
        }
    }
}
